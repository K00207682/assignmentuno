#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include "Game.h"
#include "Map.h"
#include <list>
#include <iostream>
#include <vector>
#include <ctime>
#include <string>

using namespace std;

//Construct Game Object
Game::Game()
{
}

//Destruct Game Object
Game::~Game()
{
}

void Game::init()
{
	

	m_enemy1 = new Enemy();
	m_enemy2 = new Enemy();
	m_enemy3 = new Enemy();
	m_enemy4 = new Enemy();
	m_player = new Player();
	vector<int> enemyStats = generateValues();

	//Generate a knight enemy
	m_enemy1->spawn("Knight", 40, 1, 5, 0, enemyStats[1], enemyStats[2]);
	enemyStats.clear();
	enemyStats = generateValues();
	//Generate a Paladin enemy with randomized statistics
	m_enemy2->spawn("Paladin", 50, 1, 5, 0, enemyStats[1], enemyStats[2]);
	enemyStats.clear();
	enemyStats = generateValues();
	//Generate a Squire enemy with randomized statistics
	m_enemy3->spawn("Squire", 30, 1, 5, 0, enemyStats[1], enemyStats[2]);
	enemyStats.clear();
	enemyStats = generateValues();
	//Generate a Hound enemy with randomized statistics
	m_enemy4->spawn("Drunk Knight", 30, 2, 5, 0, enemyStats[1], enemyStats[2]);
	enemyStats.clear();
	enemyStats = generateValues();
	//Generate the player with set statistics
	m_player->spawn("Unnamed Protagonist", 130, 1, 10, 5, 0, 0);

	//list code experimentation

	m_gameObjectsList.push_back(m_player);

	m_gameObjectsList.push_back(m_enemy1);
	m_gameObjectsList.push_back(m_enemy2);
	m_gameObjectsList.push_back(m_enemy3);
	m_gameObjectsList.push_back(m_enemy4);

}
void Game::draw()
{
	vector<GameObject*>::const_iterator iter;

	for (iter = m_gameObjectsList.begin(); iter != m_gameObjectsList.end(); iter++) {
		(*iter)->draw();
	}
}

void Game::update()
{
	vector<GameObject*>::const_iterator iter;

	for (iter = m_gameObjectsList.begin(); iter != m_gameObjectsList.end(); iter++) {
		(*iter)->update();
	}
}

void Game::battle()
{
	vector <GameObject*>::const_iterator iter;

	int pX;
	int pY;
	int pHealth;
    
	//Use getter method to save to local variabes for comparison 
	pX = m_gameObjectsList.front()->returnX();
	pY = m_gameObjectsList.front()->returnY();
	pHealth = m_gameObjectsList.front()->returnHealth();

		for (iter = m_gameObjectsList.begin(); iter != m_gameObjectsList.end(); iter++)//Search through game object
		{
			if ((*iter)->returnType() != "player" && (pX == (*iter)->returnX()) && (pY == (*iter)->returnY()))
			{
				//If game objects name is not the player (i.e. enemy) and X/Y coordinates are equal 
				if ((*iter)->returnType() != "player" && (pX == (*iter)->returnX()) && (pY == (*iter)->returnY()))
				{
					int playerAttack;
					cout << "\nThe protagonist engages " << (*iter)->returnType() << " in battle.\n";
					while (pHealth > 0)
					{
						cout << "Enter the number of the attack you wish to use" << endl;
						cout << "1.Slap" << endl << "2.Punch" << endl << "3.Heal" << endl << "4.Stab" << endl;
						cin >> playerAttack;
					}
					if (playerAttack == 1)
					{
						(*iter)->returnHealth() - 5;
						cout << "You slapped the enemy into next week, kind of. You dealt 5 damage to the enemy.";
					}
					if (playerAttack == 2)
					{
						(*iter)->returnHealth() - 7;
						cout << "You punched the enemy with all your might. You dealt 7 damage to the enemy.";
					}
					if (playerAttack == 3 /*&& (*iter)->potions() == true*/)
					{
						pHealth + 10;
						cout << "You consumed a potion and regained 10 health points";
					}
					if (playerAttack == 4)
					{
						(*iter)->returnHealth() - 15;
						cout << "You stabbed the enemy with your trusty sword. You dealt 15 damage.";
					}
				}
				//If player health is greater than enemy health
				if (pHealth > (*iter)->returnHealth())
				{
					//execute kill code which sets enemy health to 0
					cout << "Enemy slain!" << (*iter)->returnType() << " died! You found an ingredient for the cure to your poison on them.\n";
					(*iter)->kill();
				}
				else
				{
					//Otherwise player died
					cout << "The hero dies in an open field, slain by those he hunted.\n";
					m_gameObjectsList.front()->kill();
			}
		}
	}
}


void Game::info()
{
	vector<GameObject*>::const_iterator iter;

	for (iter = m_gameObjectsList.begin(); iter != m_gameObjectsList.end(); iter++) {
		(*iter)->info();
	}
}

void Game::grimReaper()
{
	vector<GameObject*>::const_iterator iter;

	for (iter = m_gameObjectsList.begin(); iter != m_gameObjectsList.end();) {

		bool isActive = (*iter)->isAlive();
		if (!isActive)
		{
			//	std::list<GameCharacterObject*>::iterator new_it=it; 

			iter = m_gameObjectsList.erase(iter++); //increment the iterator but return the original value ie use the old value

		}
		else
		{
			iter++; //increment the iterator and return the incremented value
		} 
	}
}

//Creates random numbers and returns them for use with the enemy objects
vector<int> Game::generateValues() 
{
	//Vector containing the random x and y coordinates
	vector<int> enemyStats; 
    //fixed vector subscript out of range by using this pushback to increase the size of my vector
	enemyStats.push_back(rand() % 10);

	//Randomize the enemy location.
	int randX = rand();
	int enemyXpos = (randX % 23) + 1;

	int randY = rand();
	int enemyYpos = (randY % 23) + 1;

	//Push results onto vector
	enemyStats.push_back(enemyXpos);
	enemyStats.push_back(enemyYpos);
	vector<int>::const_iterator iter;

	return enemyStats;
}

