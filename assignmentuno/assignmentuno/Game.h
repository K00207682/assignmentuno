#ifndef GAME_H
#define GAME_H
#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include "map.h"
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Game
{

public:
	GameObject* m_player;
	GameObject* m_enemy1;
	GameObject* m_enemy2;
	GameObject* m_enemy3;
	GameObject* m_enemy4;

	//list<GameObject*> m_gameObjectsList;

	Game(); //Construct game
	~Game(); //Destruct game
	//Vector of Game Object pointers 
	vector<GameObject*> m_gameObjectsList; 
	vector<int*> randomValues;
	vector<int> enemyStats;

	// simply set the running variable to true
	void init();//creates each object and assigns coordinates to enemy and player
	void draw();
	void update();
	void battle();
	void info();
	void grimReaper();
	vector<int> generateValues();
};
#endif /* defined(__Game__) */



