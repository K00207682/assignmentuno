#include <iostream>
#include <string>
#include "GameObject.h"

using namespace std;

GameObject::GameObject()
{
}

GameObject::~GameObject()
{
}

void GameObject::update()
{
	std::cout << "Game Character Object generic update function which is virtual " << std::endl;
}

void GameObject::spawn(string typeID, int health, int speed, int attack, int luck, int x, int y)
{

	m_typeID = typeID;
	m_health = health;;
	m_speed = speed;;
	m_attack = attack;
	m_luck = luck;
	m_x = x;
	m_y = y;
}

//Displays coordinates of each object on the grid as well as their type id
void GameObject::draw()
{
	cout << "Type ID: " << m_typeID << ". X: " << m_x << ". Y: " << m_y << "\t\n";

}

//Displays health, speed, type id and coordinates of the object
void GameObject::info()
{
	std::cout << "Type ID: " << m_typeID << " Health " << m_health << " speed " << m_speed <<
		" attack " << m_attack << " luck " << m_luck << " xpos " << m_x << " ypos " << m_y << std::endl;
}
//Calculates whether player is still alive by the end of the turn, returning the correct response depending
//on their health
bool GameObject::isAlive()
{
	if (m_health>0)
	{
		return true;
	}
	else
	{
		cout << "The unnamed protagonist is dead but not forgotten. His tale will be told for years to come." << endl;
		return false;
	}
}
//Event will display a random event to screen as the game runs
void GameObject::event()
{
	int eventRandomiser = rand();
	int eventNum = (eventRandomiser % 5);
	char playerAnswer;
	char playerAnswer2;
	string playerAnswer3;
	if (eventNum == 0)
	{
		cout << "\nYou find a green little man sitting in a ditch, -Want some supplies?- he asks. " << endl;
		cout << "How do you respond? y or n?" << endl;
		cin >> playerAnswer;
		if (playerAnswer == 'y' || playerAnswer == 'Y' || playerAnswer == 'yes')
		{
			cout << "\n-Come closer then sir-the little man grins." << endl;
			cout << "As you approach, he hands over a flask. He bids you farewell" << endl;
		}
		else if (playerAnswer == 'n' || playerAnswer == 'N')
		{
			cout << "\nThe little man disappears in a puff of green smoke, never to be seen again." << endl;
		}
	}
	if (eventNum == 1)
	{
		cout << "\nYou have stumbled upon a small hole in the ground. You might be able to climb down." << endl;
		cout << "Will you attempt it? y or n" << endl;
		cin >> playerAnswer;
		if (playerAnswer == 'y' || playerAnswer == 'Y' || playerAnswer == 'yes')
		{
			cout << "\nYou manage to squeeze down through the hole, only to start falling." << endl;
			cout << "It seems to be unending, I guess you died?" << endl;
			m_health = m_health - 1000;
			isAlive();
		}
		else if (playerAnswer == 'n' || playerAnswer == 'N')
		{
			cout << "\nYou keep moving.";
		}
	}
	if (eventNum == 2)
	{
		cout << "\nA woman runs towards you, screamingg and falls to the ground." << endl;
		cout << "-They're going to kill me!-she sobs, looking terrified." << endl;
		cout << "Do you help her? y/n" << endl;
		cin >> playerAnswer;
		if (playerAnswer == 'y' || playerAnswer == 'Y' || playerAnswer == 'yes')
		{
			cout << "\nYou stand in front of her, ready to fend off her attackers." << endl;
			cout << "A group of hooded and cloaked figures seemingly glide towards you." << endl;
			cout << "As you prepare to swing your sword, an crossbow bolt flies past your head, landing inches next to the woman." << endl;
			cout << "You swing as the group reaches you, however your blade passes through them, as if they didn't exist." << endl;
			cout << "The leader steps forward and stabs through you, killing the woman but not even scratching you." << endl;
			cout << "They disappear in front of you and as you turn to the woman, she vanishes too. Spooky." << endl;
		}
		else if (playerAnswer == 'n' || playerAnswer == 'N')
		{
			cout << "\nYou walk away, hearing a scream of pain behind you as her pursuers catch her. " << endl;
		}
	}
	if (eventNum == 3)
	{
		cout << "\nIn front you, in a small clearing, two trolls sit drinking from flasks. There is a caravan next to them, filled with riches." << endl;
		cout << "They notice you and stand up, looking suspiciously at you. -Are you here to rob us too?-the smaller troll asks." << endl;
		cout << "How do you respond? y or n?" << endl;
		cin >> playerAnswer;
		if (playerAnswer == 'y' || playerAnswer == 'Y' || playerAnswer == 'yes')
		{
			cout << "\nThen you will die too!" << endl;
			cout << "They charge at you, and before you can react, they squash you between their clubs, killing you." << endl;
			m_health = m_health - 1000;
			isAlive();
		}
		else if (playerAnswer == 'n' || playerAnswer == 'N')
		{
			cout << "The trolls offer you some special ale, to which you accept, gaining +1 attack and two new friends." << endl;
			cout << "You depart the next day with a slight hangover, leaving the two trolls with their caravan of gold." << endl;
		}
	}
	if (eventNum == 4)
	{
		cout << "\nYou meet a group of kids playing in the village before you." << endl;
		cout << "They ask if you're a hero of some kind." << endl;
		cout << "How do you respond? y/n?" << endl;
		cin >> playerAnswer;
		if (playerAnswer == 'y' || playerAnswer == 'Y' || playerAnswer == 'yes')
		{
			cout << "\nThey stare in amazement and ask lots of questions." << endl;
			cout << "An hour later, you finally escape the bombardment of questions and leave behind your new fan group." << endl;
		}
		else if (playerAnswer == 'n' || playerAnswer == 'N')
		{
			cout << "They lose interest in you and go back to their game." << endl;
		}
	}
	if (eventNum == 5)
	{
		cout << "\nYou stumble upon a graveyard. Skeletons emerge from the shadows." << endl;
		cout << "They seem angry that you've trespassed on the graveyard." << endl;
		cout << "Do you leave? y or n?" << endl;
		cin >> playerAnswer;
		if (playerAnswer == 'y' || playerAnswer == 'Y' || playerAnswer == 'yes')
		{
			cout << "\n-You leave and the skeletons return to sleep." << endl;
		}
		else if (playerAnswer == 'n' || playerAnswer == 'N')
		{
			cout << "The skeletons, in anger, charge towards you and attack, dealing no damage as their arm bones fall off." << endl;
			cout << "You walk past them as they fall apart, their embarassment following them back to the afterlife.";
		}
	}
}
//Get methods
string GameObject::returnType()
{
	return m_typeID;
}
int GameObject::returnX()
{
	return m_x;
}
int GameObject::returnY()
{
	return m_y;
}
int GameObject::returnHealth()
{
	return m_health;
}

//Set methods
void GameObject::kill()
{
	m_health = 0;
}

void GameObject::heal()
{
	//mHealth + 10;
	cout << "You healed yourself for 20 health points.";
}