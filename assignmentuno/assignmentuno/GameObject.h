#ifndef GameObject_h
#define GameObject_h
#include <string>
#include <vector>
#include <iostream>

using namespace std;

class GameObject
{
public:
	GameObject();//Construct GameObject
	~GameObject();//Destruct GameObject

	void spawn(string typeID, int health, int speed, int attack, int luck, int x, int y);
	void draw(); //Call the draw function which displays coordinates of enemy and player objects as well as their typeids
	virtual void update();
	void info();//print object information
	bool isAlive();//return whether player is still alive or not based on what health value is equal to
	void event();
	string returnType();
	int returnX();
	int returnY();
	int returnHealth();
	void heal();
	//Set values
	void kill();

protected:

	string m_typeID;
	int m_health;
	int m_speed;
	int m_attack;
	int m_luck;
	int m_x;
	int m_y;
};
#endif /* defined(__SDL_Game_Programming_Book__Chapter_3_GameObject__) */