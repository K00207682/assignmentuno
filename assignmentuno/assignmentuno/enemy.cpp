#include <iostream>
#include <string>
#include "Enemy.h"

using namespace std;

//Enemy constructor
Enemy::Enemy()
{
}

//Enemy destructor
Enemy::~Enemy()
{
}

/*To make enemy move at random, this function will update the enemy x and y axis at random
  while keeping them within the confines of the map
*/
void Enemy::update()
{
	int enemyMoveNum = rand();
	int enemyMove = (enemyMoveNum % 4);

	if (enemyMove == 0)
	{
		//increase y axis by speed if enemy moves north
		m_y += m_speed;
	}
	else if (enemyMove == 1)
	{
		//decrease x axis by speed if enemy moves east
		m_x -= m_speed;
	}
	else if (enemyMove == 2)
	{
		//increase x axis by speed if enemy moves west
		m_x += m_speed;
	}
	else if (enemyMove == 3)
	{
		//decrease y axis by speed if enemy moves south
		m_y -= m_speed;
	}

	/*
	If enemy moves outside the border of the grid, return them
	to 0,0, opposite side of where player is placed when they do 
	the same
	*/
	if ((m_x < 0 || m_x > 23) || (m_y < 0 || m_y > 23))
	{
		m_x = 0;
		m_y = 0;
		m_health -= (m_speed * 2);
	}
}