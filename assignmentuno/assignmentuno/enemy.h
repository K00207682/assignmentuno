#ifndef Enemy_h
#define Enemy_h

#include <iostream>
#include "GameObject.h"

class Enemy : public GameObject
{
public:
	Enemy(); //Construct Enemy
	~Enemy(); //Destruct Enemy

	void update();
};

#endif