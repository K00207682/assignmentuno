#include "Game.h"
#include "GameObject.h"
#include "Map.h"
#include <Windows.h>
#include <stdio.h>
#include <iostream>

using namespace std;

// our Game object
//Create new game, map and gameobject objects.
GameObject* new_event = 0;
Map* new_map = 0;
Game* g_game = 0; // creates a new game object
int main(int argc, char* argv[])
{
	//	AllocConsole();
	//	freopen("CON","w", stdout);
	
	g_game = new Game();
	new_map = new Map();
    new_event = new GameObject();

	//The poison back story gives the player purpose for hunting down the guards.
	cout << "~So begins, the epic adventure of unnamed protagonist, a name well known throughout the lands of the Fane." << endl;
	cout << "~You have walked for days, fending off bandit attacks, protecting innocent towns and dining with lords." << endl;
	cout << "~However you have been poisoned by an unknown enemy." << endl;
	cout << "~Upon investigation, you realise your water tankard was the tool in your poisoning and remember the shady guards who had given you the drink." << endl;
	cout << "~Through some interrogation, you gain the guards patrol map." << endl;
	cout << "~The cure will be on one of those guards, time to hunt." << endl; 
	//Call init function to create player and enemy objects.
	g_game->init();
	//Loop the game functions 20 times, ending loop when i reaches 20.
	for (int i = 0; i < 20; i++)
	{
		
		g_game->draw();
		g_game->update();
		g_game->info();
		g_game->grimReaper();
		getchar();
		//When the player reaches 5 turns, call a random event to present the player with optional small adventures
		if (i==5)
		{
		//Check to see if player is still alive after event
		new_event->event();
		//new_event->isAlive();
		}
	}
	//End game
	cout << "The battle is over!" << endl << endl;
	g_game->info();
	cout << "To restart, type r, to exit, type x" << endl;
	char playerMenuInput;
	cin >> playerMenuInput;
	//Attempted to create a restart function but settled for a text display instead.
	if (playerMenuInput = 'r')
	{
		cout << "You Quit.";
	}
	else
	{
		getchar();
		return 0;
	}
}