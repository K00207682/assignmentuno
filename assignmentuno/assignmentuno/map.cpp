#include <string>
#include "Map.h"
#include <iostream>
#include <vector>

//Map creates a multidimensional array called map which will be used to create our ascii map.
using namespace std;

char map[10][10];/* = {
	{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23} ,
	{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23} ,
	{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23} ,

}*/

Map::Map()
{
}

Map::~Map()
{
}

void Map::createMap()
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10;)
		{
			map[i][j] = '-';
		}
	}
}

void Map::displayMap()
{
	cout << " 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24 " << endl;
	cout << " ______________________________________________________________ " << endl;
	for (int i = 0; i < 10; i++)
	{
		cout << " " << i << "|" << map[i][0];
		for (int j = 1; j < 10; j++)
		{
			//
			cout << " " << map[i][j];
		}
		cout << "|" << endl;
	}
	cout << " _______________________________________________________________ " << endl;
}