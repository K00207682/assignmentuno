#ifndef Map_h
#define Map_h

#include <iostream>
#include "GameObject.h"
#include "Game.h"

//Create class map of base class GameObject

class Map : public GameObject
{
public:
	Map();
	~Map();

	void createMap();
	void displayMap();
	//void redisplayMap(p);

private:
	char map[10][10];
};
#endif//MAP!