#include <string>
#include "Player.h"
#include <iostream>

using namespace std;
Player::Player()
{
}

Player::~Player()
{
}

/*Update printing out how to play, taking in user movement input and storing it as a variable,
  If, Else If statements take into account the player movement choices and moves the player
  */
void Player::update()
{
	char playerInput;
	cout << "\nTo move, type W, A, S or D.\nW = Up, A = Left, S = Down and D = Right";
	cin >> playerInput;
	playerInput = toupper(playerInput);
	//m_health = m_health - 10;

	if (playerInput == 'W')
	{
		//Adding speed to the y axis so player moves up
		m_y += m_speed;
		cout << "\nYou are headed north." << endl;
	}

	else if (playerInput == 'A')
	{
		//Decreasing the x axis by speed so player moves left
		m_x -= m_speed;
		cout << "\nYou are headed west." << endl;
	}

	else if (playerInput == 'S')
	{
		//Decreasing the y axis by speed so player moves down
		m_y -= m_speed;
		cout << "\nYou are headed south.";
	}

	else if (playerInput == 'D')
	{

		//Increasing the x axis by speed so player moves right
		m_x += m_speed;
		cout << "\nYou are headed east.";
	}
	else
	{
		cout << "\nYou have not entered a movement value unnamed protagonist.";
		//Call Update Function
		update();
	}
	m_health -= m_speed;
	//If player reaches the border of the grid, either on x or y axis
	//Return them to a valid location and decrease health by a certain value.
	if ((m_x < 0 || m_x > 23 || m_y < 0 || m_y > 23))
	{
		m_x = 23;
		m_y = 23;
		m_health = m_health - 10;
	}
}