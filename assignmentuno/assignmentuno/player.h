#ifndef Player_h
#define Player_h

#include <iostream>
#include "GameObject.h"


class Player : public GameObject
{
public:
	Player();//Construct Player
	~Player();//Destruct Player

	void update();

};

#endif// !PLAYER_H